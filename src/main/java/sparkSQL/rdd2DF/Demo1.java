package sparkSQL.rdd2DF;

import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;

import java.io.Serializable;

/**
 * @author wanyx
 * @description
 * @date 2021-11-09
 */
public class Demo1 {

    public static void main(String[] args) {
        SparkSession spark = SparkSession.builder().master("local[*]").appName("rdd2DFDemmo1").getOrCreate();
        SparkContext sc = spark.sparkContext();

        RDD<String> rdd = sc.textFile("data/input/person.txt", 4);
        JavaRDD<String> javaRDD = rdd.toJavaRDD();

        JavaRDD<Person> map = javaRDD.map(line -> {
            String[] split = line.split(" ");
            return new Person(Integer.valueOf(split[0]), split[1], Integer.valueOf(split[2]));
        });



        Dataset<Person> dataset = spark.createDataset(map.rdd(), Encoders.bean(Person.class));
        Dataset<Row> df = dataset.toDF();
        dataset.createOrReplaceTempView("t_person");


        spark.sql("select name,age from t_person ").show();

        spark.sql("select name,age+1 age from t_person ").show();

        //  ========== DSL ============= 面向对象的SQL
        df.select("name", "age").show();
        // 注意：$符是把Cloumn列明转换成了Cloumn对象
        //        dataset.select($"name", &"age" + 1).show();
        // 注意：' 符是把Cloumn列明转换成了Cloumn对象
        //        dataset.select('name, 'age + 1).show();
        df.filter("age>=30").show();
//        df.filter($"age" >= 25).show();


        // 自定义udf函数
        spark.udf().register("small2big",
                new UDF1<String, String>() {
                    @Override
                    public String call(String s) throws Exception {
                        return s.toUpperCase();
                    }
                }, DataTypes.StringType);
        spark.sql("select name,small2big(name) bigName from t_person ").show();

//        dataset.schema();
//        dataset.show();

        spark.stop();
    }


    public static class Person implements Serializable {
        private Integer id;
        private String name;
        private Integer age;

        public Person(Integer id, String name, Integer age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }
}
