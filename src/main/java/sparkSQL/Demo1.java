package sparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * @author wanyx
 * @description
 * @date 2021-11-04
 */
public class Demo1 {
    public static void main(String[] args) {
        SparkSession sc = SparkSession.builder().master("local[*]").appName("Demo1").getOrCreate();
        Dataset<Row> df = sc.read().json("D:\\big_data\\data\\21-10-08\\01\\FlumeData.1633626000575");

        // 打印数据集
//        df.limit(10).show();

        // 以树形结构打印，便于查询数据结构及数据类型
//        df.printSchema();

        // 显示指定的列
//        df.select("realip").distinct().limit(10).show();

//        df.select("realip", "size").limit(10).show();

//        df.groupBy("realip").count().sort("count").limit(10).show();

        // todo：SparkSession上的SQL函数使应用程序以编程方式运行SQL查询，并将结果返回为DataSet <Row>
        // 将DataFrame注册为SQL临时视图
        df.createOrReplaceGlobalTempView("people_table");

        sc.sql("select * from global_temp.people_table limit 5").show();


    }
}
