package sparkSQL;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

/**
 * @author wanyx
 * @description
 * @date 2021-11-05
 */
public class JDBCDemo2 {
    public static void main(String[] args) {
        SparkSession sc = SparkSession.builder().master("local[*]").appName("JDBCDemo2").getOrCreate();

        Dataset<Row> dataset = sc.read().format("jdbc")
                .option("url", "jdbc:mysql://localhost:3306/ddb_platform?characterEncoding=utf8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8")
                .option("dbtable", "daily_record_error_info")
                .option("user", "root")
                .option("password", "root")
                .option("driver","com.mysql.cj.jdbc.Driver")
                .load();

            dataset.createOrReplaceTempView("error_code_info");

        Dataset<Row> sql = sc.sql("select * from error_code_info where type=\"SLOW_RESPONSE\" order by num desc limit 10");
        sql.show();


    }
}
