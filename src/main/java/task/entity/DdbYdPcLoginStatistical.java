package task.entity;


import java.io.Serializable;

/**
 * 园丁登录埋点
 *
 */
public class DdbYdPcLoginStatistical implements Serializable{
    private static final long serialVersionUID = -7164875018965464292L;
    private String userId;
    private long loginTimeInMs;
    // 系统类型
    private String systemType;
    // 电脑品牌
    private String computerBrand;
    // 登录方式（ bluetooth、authorizationCode、smartPen）
    private LoginMethod loginMethod;
    // 系统版本号
    private String systemVersion;
    // 登录状态
    private boolean loginSuccess;
    // 省
    private String province;
    // 市
    private String city;
    // phone
    private String phone;
    private long createTimeInMs;
    private long updateTimeInMs;
    
    
    public String getComputerBrand() {
        return computerBrand;
    }
    public void setComputerBrand(String computerBrand) {
        this.computerBrand = computerBrand;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public long getLoginTimeInMs() {
        return loginTimeInMs;
    }
    public void setLoginTimeInMs(long loginTimeInMs) {
        this.loginTimeInMs = loginTimeInMs;
    }
    
    public long getCreateTimeInMs() {
        return createTimeInMs;
    }
    public void setCreateTimeInMs(long createTimeInMs) {
        this.createTimeInMs = createTimeInMs;
    }
    public long getUpdateTimeInMs() {
        return updateTimeInMs;
    }
    public void setUpdateTimeInMs(long updateTimeInMs) {
        this.updateTimeInMs = updateTimeInMs;
    }
    public String getSystemType() {
        return systemType;
    }
    public void setSystemType(String systemType) {
        this.systemType = systemType;
    }
    
    public LoginMethod getLoginMethod() {
        return loginMethod;
    }
    public void setLoginMethod(LoginMethod loginMethod) {
        this.loginMethod = loginMethod;
    }
    public String getSystemVersion() {
        return systemVersion;
    }
    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }
    public boolean isLoginSuccess() {
        return loginSuccess;
    }
    public void setLoginSuccess(boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }
    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
