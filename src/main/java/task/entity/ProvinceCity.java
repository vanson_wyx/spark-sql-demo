package task.entity;

import java.io.Serializable;

/**
 * 省市信息
 * 
 */
public class ProvinceCity implements Serializable {
    private static final long serialVersionUID = -8905009818024047907L;
    private String province;
    private String city;

    public ProvinceCity() {
        super();
    }

    public ProvinceCity(String province, String city) {
        super();
        this.province = province;
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
