package task.entity;

import java.io.Serializable;

public class YdPcRecord implements Serializable{
    /**
     * 
     */
    private static final long serialVersionUID = -1887312567309118831L;
    private String type;
    private String extra;
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getExtra() {
        return extra;
    }
    public void setExtra(String extra) {
        this.extra = extra;
    }
}
