package task.entity;

public enum LoginMethod {
    BLUETOOTH,AUTHORIZATION_CODE,SMART_PEN,AUTHORIZATION,WIFI,WEIXIN
}
