package task.entity;

import java.io.Serializable;

/**
 * nginx请求信息
 *
 */
public class NginxInfo implements Serializable{
    private static final long serialVersionUID = 7150560629088065764L;
    private String realip;
    private String host;
    private String req_body;
    private String uri;
    public String getRealip() {
        return realip;
    }
    public void setRealip(String realip) {
        this.realip = realip;
    }
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public String getReq_body() {
        return req_body;
    }
    public void setReq_body(String req_body) {
        this.req_body = req_body;
    }
    public String getUri() {
        return uri;
    }
    public void setUri(String uri) {
        this.uri = uri;
    }

}
