package task.entity;
/**
 * 园丁pc统计bean
 *
 */

import java.io.Serializable;
import java.util.List;

public class YdPcStatistical implements Serializable{
    private static final long serialVersionUID = -5940548587211625425L;
    private List<YdPcRecord> records;
    private String ip;
    
    public String getIp() {
        return ip;
    }
    public void setIp(String ip) {
        this.ip = ip;
    }
    public List<YdPcRecord> getRecords() {
        return records;
    }
    public void setRecords(List<YdPcRecord> records) {
        this.records = records;
    }
  
    
    
        
}
