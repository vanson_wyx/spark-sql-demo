package task;

import com.google.gson.Gson;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;
import task.entity.*;
import task.util.StringUtil;

import java.util.*;

/**
 * @author wanyx
 * @description
 * @date 2021-11-05
 */
public class DdbYdPcLoginStatisticalStream {
    public static final Gson GSON = new com.google.gson.Gson();

    public static void main(String[] args) {
        SparkSession sc = SparkSession.builder().master("local[*]").appName("DdbYdPcLoginStatisticalStream").getOrCreate();

        Dataset<Row> dataset = sc.read().json("D:/big_data/data/21-10-21/*");

        dataset.createOrReplaceTempView("yd_log");

        Dataset<Row> rowDataset = sc.sql("select * from yd_log where uri=\"/v1/logs/record/yd\"").toDF();

        Dataset<NginxInfo> ds1 = rowDataset.as(Encoders.bean(NginxInfo.class));

        JavaRDD<NginxInfo> rdd = ds1.toJavaRDD().filter(Objects::nonNull);

        JavaRDD<DdbYdPcLoginStatistical> flatMapRDD = rdd.map(new Function<NginxInfo, YdPcStatistical>() {
            @Override
            public YdPcStatistical call(NginxInfo nginxInfo) throws Exception {
                try {
                    YdPcStatistical fromJson = GSON.fromJson(nginxInfo.getReq_body(), YdPcStatistical.class);
                    fromJson.setIp(nginxInfo.getRealip());
                    return fromJson;

                } catch (Exception e) {

                }
                return null;

            }
        }).filter(new Function<YdPcStatistical, Boolean>() {
            private static final long serialVersionUID = -4792560274271878179L;

            @Override
            public Boolean call(YdPcStatistical v1) throws Exception {
                if (Objects.isNull(v1)) {
                    return false;
                }
                // 过滤规则
                List<YdPcRecord> records = v1.getRecords();
                for (YdPcRecord recordsBean : records) {
                    if ("login".equals(recordsBean.getType())) {
                        return true;
                    }

                }
                return false;
            }
        }).flatMap(new FlatMapFunction<YdPcStatistical, DdbYdPcLoginStatistical>() {
            private static final long serialVersionUID = -3118031763817130363L;

            @Override
            public Iterator<DdbYdPcLoginStatistical> call(YdPcStatistical t) throws Exception {
                final List<YdPcRecord> records = t.getRecords();
                final List<DdbYdPcLoginStatistical> ddbYdPcLoginStatisticals = new ArrayList<DdbYdPcLoginStatistical>();
                ProvinceCity dealIpAddr = StringUtil.dealIpAddr(t.getIp());
                for (YdPcRecord ydPcRecord : records) {
                    if ("login".equals(ydPcRecord.getType())) {
                        DdbYdPcLoginStatistical fromJson = GSON.fromJson(ydPcRecord.getExtra(), DdbYdPcLoginStatistical.class);
                        long currentTimeMillis = System.currentTimeMillis();
                        fromJson.setCreateTimeInMs(currentTimeMillis);
                        fromJson.setUpdateTimeInMs(currentTimeMillis);
                        fromJson.setProvince(dealIpAddr.getProvince());
                        fromJson.setCity(dealIpAddr.getCity());
                        ddbYdPcLoginStatisticals.add(fromJson);
                    }
                }
                return ddbYdPcLoginStatisticals.iterator();
            }
        });

        Dataset<DdbYdPcLoginStatistical> scDataset = sc.createDataset(flatMapRDD.rdd(), Encoders.bean(DdbYdPcLoginStatistical.class));
        // coalesce 重新设置分区数
        scDataset.coalesce(1).write().mode(SaveMode.Append)
                .format("jdbc")
                .option("url", "jdbc:mysql://localhost:3306/ddb_platform?characterEncoding=utf8&useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8")
                .option("dbtable", "ddb_yd_pc_login_statistical")
                .option("user", "root")
                .option("password", "root")
                .option("driver", "com.mysql.cj.jdbc.Driver")
                .saveAsTable("ddb_yd_pc_login_statistical");
    }
}
