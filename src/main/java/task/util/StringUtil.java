package task.util;

import task.entity.ProvinceCity;

/**
 * @author wanyx
 * @description
 * @date 2021-11-05
 */
public class StringUtil {
    /**
     * 根据ip获取省市信息
     */
    public static ProvinceCity dealIpAddr(String ipAddr) {

        final ProvinceCity provinceCity = new ProvinceCity();
        provinceCity.setProvince("province");
        provinceCity.setCity("city");
        return provinceCity;
    }
}
