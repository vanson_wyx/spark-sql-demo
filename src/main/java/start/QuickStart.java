package start;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

/**
 * @author wanyx
 * @description
 * @date 2021-11-04
 */
public class QuickStart {
    public static void main(String[] args) {
//        String filePath = "D:/big_data/data/README.md";
        String filePath = "hdfs://192.168.1.28:9000/user/logsystem/nginx-log/21-07-29/*";
        SparkSession sc = SparkSession.builder().master("local[*]").appName("QuickStart").getOrCreate();
        Dataset<String> ds = sc.read().textFile(filePath);

        long a = ds.filter((FilterFunction<String>) s -> s.contains("a")).count();
        long b = ds.filter((FilterFunction<String>) s -> s.contains("b")).count();

        System.out.println("Lines with a: " + a + ", lines with b: " + b);
        sc.stop();

    }
}
